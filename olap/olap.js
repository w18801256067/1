layui.use(['transfer', 'layer', 'util','form', 'layedit', 'laydate','table'], function(){
  var $ = layui.$
  ,transfer = layui.transfer
  ,layer = layui.layer
  ,util = layui.util
  ,form = layui.form
  ,layedit = layui.layedit
  ,table  =  layui.table
  ,laydate = layui.laydate;
  
  //穿梭框数据
  var data1 = [
    {"value": "jobTitle", "title": "1"}
    ,{"value": "jobSalart", "title": "杜甫"}
    ,{"value": "jobSalartsAVG", "title": "苏轼"}
    ,{"value": "jobRequire", "title": "李清照"}
    ,{"value": "jobRequireMin", "title": "鲁迅"}
    ,{"value": "jobEdu", "title": "巴金"}
    ,{"value": "jobLoction", "title": "冰心"}
    ,{"value": "jobLoctionCity", "title": "矛盾"}
    ,{"value": "jobRealseTime", "title": "贤心"}
    ,{"value": "jobRealseTimeYear", "title": "贤心"}
    ,{"value": "jobRealseTimeMonth", "title": "贤心"}
    ,{"value": "jobNumber", "title": "贤心"}
    ,{"value": "jobdetail", "title": "贤心"}
    ,{"value": "companyTitle", "title": "贤心"}
    ,{"value": "companyNature", "title": "贤心"}
    ,{"value": "companyScale", "title": "贤心"}
    ,{"value": "companyScaleLevel", "title": "贤心"}
    ,{"value": "companyLoction", "title": "贤心"}
    ,{"value": "companyIndusty", "title": "贤心"}
    ,{"value": "jobUrl", "title": "贤心"}

    ,{"value": "firstclass", "title": "贤心"}
    ,{"value": "secondclass", "title": "贤心"}
    ,{"value": "technicalPoint", "title": "贤心"}
    
  ]

  //显示搜索框
  transfer.render({
    elem: '#test4'
    ,data: data1
    ,title: ['维度', '已选中的维度']
    ,showSearch: true
	,width: 208 //定义宽度
    ,height: 250 //定义高度
    ,id: 'key123'
  })
  transfer.render({
    elem: '#test3'
    ,data: data1
    ,title: ['度量', '已选中的度量']
    ,showSearch: true
  	,width: 150 //定义宽度
    ,height: 250 //定义高度
  })
  // var getData = transfer.getData('key123');
  // console.log(JSON.stringify(getData));
  //批量办法定事件
  util.event('lay-demoTransferActive', {
    getData: function(othis){
      var getData = transfer.getData('key123'); //获取右侧数据
      layer.alert(JSON.stringify(getData)); 
      console.log(JSON.stringify(getData));
      console.log(getData);
    }
    ,reload:function(){
      //实例重载
      transfer.reload('key123', {
        title: ['文人', '喜欢的文人']
        ,value: ['jobTitle', 'jobSalart', 'jobEdu']
        ,showSearch: true
      })
    }
  });

 $.ajax({
  type:"GET",
  url:'http://120.55.103.70:9002/BIGDATE/api/info/infos',
  // dataType:"json",
  data:{
    // id:id,
    // jobTitle:jobTitle,
    // jobSalart:jobSalart,
    // jobSalartsAVG:jobSalartsAVG,
    // jobRequire:jobRequire,
    // jobRequireMin:jobRequireMin,
    // jobEdu:jobEdu,
    // jobLoction:jobLoction,
    // jobLoctionCity:jobLoctionCity,
    // jobRealseTime:jobRealseTime,
    // jobRealseTimeYear:jobRealseTimeYear,
    // jobRealseTimeMonth:jobRealseTimeMonth,
    // jobNumber:jobNumber,
    // jobdetail:jobdetail,
    // companyTitle:companyTitle,
    // companyNature:companyNature,
    // companyScale:companyScale,
    // companyScaleLeve:companyScaleLeve,
    // companyLoction:companyLoction,
    // companyIndusty:companyIndusty,
    // jobUrl:jobUrl,
    // firstclass:firstclass,
    // secondclass:secondclass,
    // technicalPoint:technicalPoint,

  },
  success:function(data1){
    console.log(data1);
    if(data1){
      var option={
        elem: '#test'
        // ,url:'http://120.55.103.70:9002/BIGDATE/api/info/infos'
     //  	,parseData: function(res){ //res 即为原始返回的数据
     //    return {
          
     //     //  "code": res[0].status, //解析接口状态
     //      "msg": res[0].message, //解析提示文本
     //      "count": res[0].total, //解析数据长度
     //      "data": res[1]//解析数据列表
     //    };
           
     //  }
        ,height:'full-20'
         ,page: true
         ,limit: 20 //每页默认显示的数量
          ,toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
        ,defaultToolbar: ['filter', 'exports', 'print', { //自定义头部工具栏右侧图标。如无需自定义，去除该参数即可
          title: '提示'
          ,layEvent: 'LAYTABLE_TIPS'
          ,icon: 'layui-icon-tips'
        }]
        ,title: '舆情信息表'
        ,cols: [[
          {type: 'checkbox', fixed: 'left', width:80}
          ,{field:'jobTitle', title:'ID', width:60, fixed: 'left', unresize: true,sort: true}
          // ,{field:'num', title:'编号', width:100, fixed: 'left', unresize: true, sort: true}
          // ,{field:'platform', title:'平台', width:80}
          // ,{field:'category', title:'类别', width:80}
          // ,{field:'company', title:'责任单位', width:90}
          ,{field:'jobSalart', title:'内容'}
          ,{field:'jobRequire', title:'网名', width:80}
          ,{field:'jobEdu', title:'来源', width:80}
          ,{field:'jobLoction', title:'星级', width:80}
          ,{field:'jobdetail', title:'处置措施'}
          // ,{field:'feedback', title:'是否反馈', width:90}
          // ,{field:'feedback_date', title:'反馈日期', width:90}
          // ,{field:'implement_feedback', title:'落实反馈',width:90}
          //,{field:'administrator_id', title:'管理员', width:80}
          //,{field:'create_time', title:'添加时间', width:120}
          //,{field:'update_time', title:'更新时间', width:120}
          ,{fixed: 'right', title:'操作', toolbar: '#barDemo', width:280}
        ]]
        

      };
      option.data=data1.data;
      table.render(option);

    }else{
      layer.msg("未获取到信息");
    }
  }

 })



//  table.render({
//    elem: '#test'
//    ,url:'http://120.55.103.70:9002/BIGDATE/api/info/infos'
// //  	,parseData: function(res){ //res 即为原始返回的数据
// //    return {
   	
// //     //  "code": res[0].status, //解析接口状态
// //      "msg": res[0].message, //解析提示文本
// //      "count": res[0].total, //解析数据长度
// //      "data": res[1]//解析数据列表
// //    };
 		 
// //  }
//  		,height:'full-20'
//     // ,width:
//     ,page: true
//     ,limit: 20 //每页默认显示的数量
//  		,toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
//    ,defaultToolbar: ['filter', 'exports', 'print', { //自定义头部工具栏右侧图标。如无需自定义，去除该参数即可
//      title: '提示'
//      ,layEvent: 'LAYTABLE_TIPS'
//      ,icon: 'layui-icon-tips'
//    }]
//    ,title: '舆情信息表'
//    ,cols: [[
//      {type: 'checkbox', fixed: 'left', width:80}
//      ,{field:'jobTitle', title:'ID', width:60, fixed: 'left', unresize: true,sort: true}
//  	  // ,{field:'num', title:'编号', width:100, fixed: 'left', unresize: true, sort: true}
//  	  // ,{field:'platform', title:'平台', width:80}
//  	  // ,{field:'category', title:'类别', width:80}
//  	  // ,{field:'company', title:'责任单位', width:90}
//  	  ,{field:'jobSalart', title:'内容'}
//      ,{field:'jobRequire', title:'网名', width:80}
//  	  ,{field:'jobEdu', title:'来源', width:80}
//  	  ,{field:'jobLoction', title:'星级', width:80}
//      ,{field:'jobdetail', title:'处置措施'}
//      // ,{field:'feedback', title:'是否反馈', width:90}
//      // ,{field:'feedback_date', title:'反馈日期', width:90}
//      // ,{field:'implement_feedback', title:'落实反馈',width:90}
//      //,{field:'administrator_id', title:'管理员', width:80}
//      //,{field:'create_time', title:'添加时间', width:120}
//  	  //,{field:'update_time', title:'更新时间', width:120}
//      ,{fixed: 'right', title:'操作', toolbar: '#barDemo', width:280}
//    ]]
   
   
//   //  ,done: function(res, curr, count){
//   //   console.log(res);
//   //   var getData = transfer.getData('key123'); //获取右侧数据
//   //   // console.log("getData",size(getData));
//   //   // console.log("size",size(res.data[0]))

//   //  //  console.log(JSON.stringify(res.data));

//   //  // if(test== 0){//此处test为你的条件值
//   //  // $("[data-field='quoted_price1']").css('display','none'); //关键代码
//   //  //       }
//   //  // if(test2== 0){//此处test2为你的条件值
//   //  // $("[data-field='quoted_price2']").css('display','none');//关键代码
//   //  //       }
//   // //  var l=0;
//   // //   for(var i=0 ;i<size(res.data[0]);i++){
//   // //     for(var j=0;j<size(getData);j++){
//   // //       if(item(res.data[0])[i].value != item(getData)[j].value){
//   // //             l++;
//   // //       }
//   // //       if(l==size(getData)-1){

          
//   // //       }
//   // //     }
//   // //   }   
//   //        }
// });



function item(res){
  for(item in res){
   
    return item;
  }
  
 }

function size(res){
 var i=0;
 for(item in res){
  
   i++;
 }
 return i;
}
 //头工具栏事件
//  table.on('toolbar(test)', function(obj){
//    var checkStatus = table.checkStatus(obj.config.id);
//    switch(obj.event){
//      case 'getCheckData':
//        var data = checkStatus.data;
//        layer.alert(JSON.stringify(data));
//      break;
//      case 'getCheckLength':
//        var data = checkStatus.data;
//        layer.msg('选中了：'+ data.length + ' 个');
//      break;
//      case 'isAll':
//        layer.msg(checkStatus.isAll ? '全选': '未全选');
//      break;
     
//  //自定义头工具栏右侧图标 - 提示
//      case 'LAYTABLE_TIPS':
//        layer.alert('这是工具栏右侧自定义的一个图标按钮');
//      break;
//    };
//  });
 
// 左侧提交全部信息
form.on('submit(demo22)', function(data) {
//  获取穿梭框数据
  var getData = transfer.getData('key123'); //获取右侧数据
  console.log("getData",getData) 
  
  

console.log("getData1",getData1)
// 获取维度选择全部数据
    var img_url1 =[];
    var i=0
    var q=0;
    var w=0;
    var r=0;
    // var region = $("select[name='quiz2']").val();
    $("select[name='quiz1']").each(function(){
      // console.log(r)
      img_url1[r] = new Map();
      // img_url.push($(this).val());
      img_url1[r][0]=$(this).val();
      r++
      // console.log("index",i)
      // console.log("$(this).val()",$(this).val())
    })
    $("select[name='quiz2']").each(function(){
      
      // img_url.push($(this).val());
      img_url1[i][1]=$(this).val();
      i++
    
      // console.log("index",i)
      // console.log("$(this).val()",$(this).val())
  })
$("select[name='quiz3']").each(function(){
  // img_url.push($(this).val());
  img_url1[q][2]=$(this).val();
  q++
  // console.log("index",i)
  // console.log("$(this).val()",$(this).val())
})
$("input[name='title1']").each(function(){
  
  // img_url.push($(this).val());
  img_url1[w][3]=$(this).val();
  w++
  // console.log("index",i)
  // console.log("$(this).val()",$(this).val())
})


//旧key到新key的映射
// var keyMap = {
//   "0" : "relation",
//   "1" : "column",
//   "2" : "symbol",
//   "3" : "manage"
// };

// for(var i = 0;i < img_url1.length;i++){
//       var obj = img_url1[i];
//       for(var key in obj){
//                  var newKey = keyMap[key];
//                  if(newKey){
//                           obj[newKey] = obj[key];
//                           delete obj[key];
//                    }
//           }
// }
   for(var i=0;i<img_url1.length;i++){
    console.log(img_url1[i])
   }
   console.log(JSON.stringify(img_url1))
  // 获取度量选择全部数据
  var img_url2 =[];
  var a=0
  var b=0;
  var c=0;
  var d=0;
  var e=0;
  // var region = $("select[name='quiz2']").val();
  $("select[name='quiz4']").each(function(){
   
    img_url2[a] = new Map();
    // img_url.push($(this).val());
    img_url2[a][0]=$(this).val();
    a++;
    // console.log("index",i)
    // console.log("$(this).val()",$(this).val())
  })
  $("select[name='quiz5']").each(function(){
    
    // img_url.push($(this).val());
    img_url2[b][1]=$(this).val();
    b++;
  
    // console.log("index",i)
    // console.log("$(this).val()",$(this).val())
})
$("select[name='quiz6']").each(function(){
// img_url.push($(this).val());
img_url2[c][2]=$(this).val();
c++;
// console.log("index",i)
// console.log("$(this).val()",$(this).val())
})
$("select[name='quiz7']").each(function(){
  // img_url.push($(this).val());
  img_url2[d][3]=$(this).val();
  d++;
  // console.log("index",i)
  // console.log("$(this).val()",$(this).val())
  })
$("input[name='title2']").each(function(){

// img_url.push($(this).val());
img_url2[e][4]=$(this).val();
e++;
// console.log("index",i)
// console.log("$(this).val()",$(this).val())
})




var getData1 = {}
for(var i=0;i<getData.length;i++){
  getData1['select['+i+'].title']=getData[i].title
  getData1['select['+i+'].value']=getData[i].value
}
console.log("img_url2",img_url2)
for(var i=0;i<img_url2.length;i++){
  console.log("item",item[0],item[1])
  getData1['bigdataReceive['+i+'].column']=img_url2[i][1]
  getData1['bigdataReceive['+i+'].relation']=img_url2[i][0]
  getData1['bigdataReceive['+i+'].symbol']=img_url2[i][3]
  getData1['bigdataReceive['+i+'].sqlFunction']=img_url2[i][2]
  getData1['bigdataReceive['+i+'].manage']=img_url2[i][4]

}

console.log("img_url1",img_url1)
for(var i=0;i<img_url1.length;i++){
  
  getData1['bigdataReceiveWhere['+i+'].relation']=img_url1[i][0]
  getData1['bigdataReceiveWhere['+i+'].column']=img_url1[i][1]
  getData1['bigdataReceiveWhere['+i+'].symbol']=img_url1[i][2]
  getData1['bigdataReceiveWhere['+i+'].manage']=img_url1[i][3]
}
var a=[]
  $.ajax({
      url:'http://120.55.103.70:9002/BIGDATE/api/info/connectInfo',
      type:'POST',
      data:getData1,
      success:function (data) {
        console.log(data);
        // option.data=data.data;
a=Object.keys(data.data[0])
table.render({
  elem: '#test'
  , url: 'http://120.55.103.70:9002/BIGDATE/api/info/connectInfo'
  //  	,parseData: function(res){ //res 即为原始返回的数据
  ,where: getData1
  ,method:'post'
  , height: 'full-20'
  // ,width:
  , toolbar: '#toolbarDemo' //开启头部工具栏，并为其绑定左侧模板
  , defaultToolbar: ['filter', 'exports', 'print', { //自定义头部工具栏右侧图标。如无需自定义，去除该参数即可
    title: '提示'
    , layEvent: 'LAYTABLE_TIPS'
    , icon: 'layui-icon-tips'
  }]
  , title: '舆情信息表'
  , cols: [[

    { type: 'checkbox', fixed: 'left', width: 80 }
    , { field: 'jobTitle', title: 'ID', width: 60, fixed: 'left', unresize: true, sort: true }
    , { field: 'jobSalart', title: '内容' }
    , { field: 'jobRequire', title: '网名', width: 80 }
    , { field: 'jobEdu', title: '来源', width: 80 }
    , { field: 'jobLoction', title: '星级', width: 80 }
    , { field: 'jobdetail', title: '处置措施' }
    , { fixed: 'right', title: '操作', toolbar: '#barDemo', width: 280 }
  ]]
  , page: true
  , limit: 15
  , done: function (res, curr, count) {
    console.log(res);
    var getData = transfer.getData('key123'); //获取右侧数据
    var b=['jobTitle','jobSalart','jobRequire','jobEdu','jobLoction','jobdetail']
    for(var i =0 ; i<b.length;i++){
      var flag=true
      for (var j=0;j<a.length;j++){
        if(a[j]==b[i]){
          flag=false
        }
      }
      if(flag){
        $("[data-field='"+b[i]+"']").css('display','none');
      }
    }
   
  }
});
      }
  })
 
  return false;
});


 //监听表格复选框选择 前面的√
 table.on('checkbox(test)', function (obj) {
 	  var data = obj.data;
     // console.log(obj)
 	  form.on('submit(demo22)', function() {
 	      $.ajax({
 	          url:'http://localhost:8080/IPOMS/AddNewFormServlet',
 	          type:'POST',
 	          data:{
 	  				  id:data.id,
 					  // num:data.num,
 					  // platform:data.platform,
 	  				//   category:data.category,
 	  				//   company:data.company,
 	  				  content:data.content,
 	  				  user_name:data.user_name,
 	  				  source:data.source,
 	  				  grade:data.grade,
 	  				  measures:data.measures,
 	  				  // feedback:data.feedback,
 	  				  // feedback_date:data.feedback_date,
 	  				  // implement_feedback:data.implement_feedback
 	  				},
 	          success:function (msg) {
 	        	  var json = JSON.parse(msg);
 	        	  var returnCode = json.returnCode;//取得返回数据（Sting类型的字符串）的信息进行取值判断
 	              if(returnCode==200){
 	                  // layer.closeAll('loading');
 	                  // layer.load(2);
 	                  layer.msg("提交成功", {icon: 6});
 					   delete obj.data;
 					  setTimeout(function(){
 							location.reload();//重新加载页面表格
 						}, 1000);
 	                  
 	              }else{
 	                  layer.msg("提交失败", {icon: 5});
 	              }
 	          }
 	      })
 	  });
 	  
 });
 
 
 
 
 
 //监听行工具事件 编辑 查看 删除
 table.on('tool(test)', function(obj){
 	  // 查看
   var data = obj.data;
   // console.log(obj)
 	if (obj.event === 'detail') {
 	    // layer.msg('ID：' + data.id + ' 的查看操作');
 		layer.open({
 		  type: 1,
 		  // content:  'ID:' + data.id+'编号:'+data.num+'平台:'+data.platform +'类别:'+data.category+'责任单位:'+data.company+'内容:'+data.content+'网名:'+data.user_name+'来源:'+data.source+'等级:'+data.grade+'研判建议:'+data.suggest+'是否反馈:'+data.feedback+'反馈日期:'+data.feedback_date+'落实反馈:'+data.implement_feedback
 		  title: "查看舆情信息",
 		  area: ['420px', ''],
 		  content: $("#popCheckTest"), //引用的弹出层的页面层的方式加载修改界面表单
 		  success:function(layero,index){
 							
 		  	                 // $('#platform').val(data.platform);
 		  	                 // $('#category').val(data.category); 
 		  	                 // $('#company').val(data.company); 
 		  					 $('#ccontent').val(data.content);
 		  					 $('#uuser_name').val(data.user_name);
 		  					 $('#ssource').val(data.source);
 		  					 $('#ggrade').val(data.grade);
 		  					 $('#mmeasures').val(data.measures);
 		  					 // $('#feedback').val(data.feedback);
 		  					 // $('#feedback_date').val(data.feedback_date);
 		  					 // $('#implement_feedback').val(data.implement_feedback);
 		                 }
 		
 		
 		});
 	} 
 	
 	// 删除
   else if(obj.event === 'del'){
     layer.confirm('真的删除行么', function(index){  
      $.ajax({
          url: "http://localhost:8080/IPOMS/DeletePartServlet",
          type: "POST",
          data: {
 			   id: data.id, 
 		    //    num: data.num,
 			   // platform:data.platform,
 			   // category:data.category,
 			   // company:data.company,
 			   content:data.content,
 			   user_name:data.user_name,
 			   source:data.source,
 			   grade:data.grade,
 			   measures:data.measures,
 			   // feedback:data.feedback,
 			   // feedback_date:data.feedback_date,
 			   // implement_feedback:data.implement_feedback
 		   },
          success: function (msg) {
              var json = JSON.parse(msg);
              var returnCode = json.returnCode;
              if (returnCode == 200) {
                  //删除这一行
                  obj.del();
                  //关闭弹框
                  layer.close(index);
                  layer.msg("删除成功", {icon: 6});
              } else {
                  layer.msg("删除失败", {icon: 5});
              }
          }
      });
      return false;
    
 	  });
   } else if(obj.event === 'edit'){
     //formData = data;
     // if (obj.event === 'del') {
     //     layer.confirm('真的删除行么', function (index) {
     //         obj.del();
     //         layer.close(index);
     //     });
     // } 
 	  // else if (obj.event === 'edit') {
         layer.open({
             //layer提供了5种层类型。可传入的值有：0（信息框，默认）1（页面层）2（iframe层）3（加载层）4（tips层）
             type: 1,
             title: "修改舆情信息",
             area: ['420px', ''],
             content: $("#popUpdateTest"), //引用的弹出层的页面层的方式加载修改界面表单
 			  success:function(layero,index){
 			  	                 // $('#platform').val(data.platform);
 			  	                 // $('#category').val(data.category); 
 			  	                 // $('#company').val(data.company); 
 								 $('#content').val(data.content);
 								 $('#user_name').val(data.user_name);
 								 $('#source').val(data.source);
 								 $('#grade').val(data.grade);
 								 $('#measures').val(data.measures);
 								 // $('#feedback').val(data.feedback);
 								 // $('#feedback_date').val(data.feedback_date);
 								 // $('#implement_feedback').val(data.implement_feedback);
 			                 }
 			  
         });
         //动态向表传递赋值可以参看文章进行修改界面的更新前数据的显示，当然也是异步请求的要数据的修改数据的获取
         setFormValue(obj,data);
     // }
   
 	}
 
 });

 //监听弹出框表单提交，massage是修改界面的表单数据'submit(demo11),是修改按钮的绑定
 function setFormValue(obj,data){
     form.on('submit(demo11)', function(massage) {
         $.ajax({
             url:'http://localhost:8080/IPOMS/UpdataPartFormServlet',
             type:'POST',
             data:{
 				  id:data.id,
 				  // platform:massage.field.platform,
 				  // category:massage.field.category,
 				  // company:massage.field.company,
 				  content:massage.field.content,
 				  user_name:massage.field.user_name,
 				  source:massage.field.source,
 				  grade:massage.field.grade,
 				  measures:massage.field.measures,
 				  // feedback:massage.field.feedback,
 				  // feedback_date:massage.field.feedback_date,
 				  // implement_feedback:massage.field.implement_feedback
 				},
             success:function (msg) {
           	  var json = JSON.parse(msg);
           	  var returnCode = json.returnCode;//取得返回数据（Sting类型的字符串）的信息进行取值判断
                 if(returnCode==200){
                     layer.closeAll('loading');
                     layer.load(2);
                     layer.msg("修改成功", {icon: 6});
                     setTimeout(function(){
                        obj.update({
 							 // platform:massage.field.platform,
 							 // category:massage.field.category,
 							 // company:massage.field.company,
 							 content:massage.field.content,
 							 user_name:massage.field.user_name,
 							 source:massage.field.source,
 							 grade:massage.field.grade,
 							 measures:massage.field.measures,
 							 // feedback:massage.field.feedback,
 							 // feedback_date:massage.field.feedback_date,
 							 // implement_feedback:massage.field.implement_feedback,
 							 
                          });//修改成功修改表格数据不进行跳转
                        location.reload();//重新加载页面表格
                          layer.closeAll();//关闭所有的弹出层
                         
                          
                     }, 1000);
                     
                 }else{
                     layer.msg("修改失败", {icon: 5});
                 }
             }
         })
     })
 
 }
   var str1="";
   var addbtn1 = document.getElementById("addbtn1");

   addbtn1.onclick = function(){
    var index1=0;
    index1++;
    var flied1 = document.getElementById("flied1");
    // var text1=""
    // str1=text1+str1
    var a=flied1.firstElementChild.cloneNode(true)
    flied1.append(a)
    layui.form.render("select");
  };
                        
  
 
  var addbtn2 = document.getElementById("addbtn2");

  addbtn2.onclick = function(){
    var index2=0;
        index2++;
   var flied2 = document.getElementById("flied2");
   // var text1=""
   // str1=text1+str1
   var b=flied2.firstElementChild.cloneNode(true)
   flied2.append(b)
   layui.form.render("select");
 };
 

  
  
});